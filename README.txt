
1- récupérer le projet sur votre poste

2- ouvrir le projet avec VScode

3- depuis l'onglet extensions de VScode, installer l'extention Live Server.

4- une fois l'extension installé, cliquer sur Go Live en bas à droite de VScode.

5- votre navigateur ouvre alors un nouvel onglet avec les résultats du code.