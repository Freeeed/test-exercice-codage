var ul = document.getElementById("numberlist"); // la variable ul va chercher l'élément possédant l'id numberlist

for (var i = 1; i <= 100; i++, msg = "") { // i = 1, et tant que i est inférrieur à 100 on l'incrémente de 1, et on affiche un message null
  if (i % 15 == 0) // si i est divisible par 15 (donc 3 et 5)
    msg = "fizzbuzz"; // on affiche le message "fizzbuzz"
  else if (i % 3 == 0) // sinon si i est divisible par 3
    msg = "fizz"; // on affiche le message "fizz"
  else if (i % 5 == 0) //sinon si i est divisible par 5 
    msg = "buzz"; // on affiche le message "fizzbuzz"
  else // sinon le message reste null
    msg = "";

  var li = document.createElement("li"); // la variable li crée une balise <li>

  li.appendChild(document.createTextNode(i + " = " + msg)); // la balise <li> aura comme texte, la valeur de la variable i suivi d'un string contenant le signe =
                                                            // et le message défini dans la variable i, s'il est divisible ou pas.
  ul.appendChild(li); // la variable li qui crée une balise <li> et un enfant de la variable ul.
}